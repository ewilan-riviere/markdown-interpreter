# **Markdown Interpreter**

[![nuxt](https://img.shields.io/static/v1?label=Nuxt&message=v3.*&color=00C58E&style=flat-square&logo=nuxt.js&logoColor=ffffff)](https://nuxtjs.org/)
[![nuxt-ssr](https://img.shields.io/static/v1?label=Designed%20to%20be&message=SSR&color=00C58E&style=flat-square&logo=nuxt.js&logoColor=ffffff)](https://nuxtjs.org/docs/concepts/server-side-rendering/)
[![tailwindcss](https://img.shields.io/static/v1?label=Tailwind%20CSS&message=v3.*&color=38B2AC&style=flat-square&logo=tailwind-css&logoColor=ffffff)](https://tailwindcss.com/)

[![node](https://img.shields.io/static/v1?label=NodeJS&message=v16.15&color=339933&style=flat-square&logo=node.js&logoColor=ffffff)](https://nodejs.org/en)
[![pnpm](https://img.shields.io/static/v1?label=pnpm&message=v7.*&color=F69220&style=flat-square&logo=pnpm&logoColor=ffffff)](https://pnpm.io)

📀 [**repository**](https://gitlab.com/ewilan-riviere/markdown-interpreter)  
💻 [**md-interpreter.git-projects.xyz**](https://md-interpreter.git-projects.xyz): demo  

This project allow user to paste markdown code into textarea and convert it to HTML code directly to clipboard.

## 🔧 Project setup

Download dependencies

```bash
pnpm i
```

Compiles and hot-reloads for development

```bash
pnpm dev
```

### 💻 Production setup

Compiles and minifies for production

```bash
pnpm build
```
